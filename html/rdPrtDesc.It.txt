rdPrtDesc:
----------

Questo programma consente l'estrazione della descrizione dei
dati della stampante necessari a ttink/mtink e li stampa in formato
leggibile.

Se dai la tua descrizione, il file di descrizione
verr� letto e potrai creare un nuovo file d con tutti
i dati rilevanti.

Sintassi: rdPrtDesc -p|-c

-p rdPrtDes crea il file leggibile che include tutte le stampanti
   conosciute.
   
-c rdPrtDes legge il file printer.desc nella directory corrente
   e genera un nuoco file c che pu� essere usato da ttink/mtink.
   I dati relativi alla stampante noti e quelli presi dal file di
   descrizione vengono messi assieme.


FILE printer.desc:
------------------

Questo file contiene blocchi blocchi che descrivono le possibilit�
della stampante.
Un blocco � cos� fatto:

.PRINTER
   .name:                Stylus C20SX
   .colorsNb:            4
   .mainProt:            D4
   .stateFlg:            True
   .exchangeFlg:         False
   .exchangeSeparateFlg: False
   .cleanSeparateFlg:    True
   .resetProt:           D4
   .alignProt:           OLD D4
   .idFlg:               True
   .passesNb:            3
   .choicesNb:           15
   .colorPassesNb:       2
   .colorChoicesNb:      9
   .alignFunctionName:   -
.END


La parola .PRINTER indica che inizia una nuova descrizione. Le parole
successive, fino ad .END, essa esclusa, descrivono la possibilit�
della stampante.

Non iniziare una linea con un punto '.': questo carattere indica a
ttink/mtink ed al programma di aiuto rdPrtDesc che inizia una linea di
descrizione.

La parola .name: deve contenere il nome della stampante come risulta dalla
richiesta nome stampante. 

.colorsNb: indica a ttink/mtink quanti inchiostri siano usati.

Le indicazioni che terminano Prot: indicano ai programmi quale protocollo
dev'essere usato.

Le nuove stampanti conoscono un protocollo noto come protocollo D4.
Questo nuovo protocollo permette di comunicare con la stampante tramite
canali indipendenti. Questo permette per esempio di chiedere la quantit�
di inchiostro rimasto o lo stato della stampante durante la stampa.
Il protocollo D4 include anche un insieme di comandi, di modo che la
maggior parte dei vecchi comandi ha un corrispondente. Sfortunatamente,
non tutte le stampanti offrono l'intero insieme di comandi D4.
Per questo motivo, ed anche per compatibilit� con driver che non
conoscono il protocollo D4, � possibile dire alla stampante che il
protocollo D4 dev'essere disabilitato.

Sono dunque possibili le seguenti combinazioni di valori:

- OLD       La stampante nonsa nulla di D4.
- OLD D4    Usa comandi non D4 command, ma li invia secondo il protocollo D4
- OLD EXD4  Usa i comandi normali e configura la stampante in modalit�
            compatibile.
- D4        La stampante conosce un comando speciale D4, usalo.

La parola .mainProt: indica la modalit� di default.

La parola .resetProt: indica il tipo di protocollo top usato per il comando
                      reset. Le stampanti D4 possono conoscere questo comando
                      ma non eseguire l'operazione come richiesto
                      (es. Stylus Color 980 o Stylus Scan ...)

La parola .alignProt: Questo comando non sembra avere un corrispondente D4
                      e viene usato il comando classico.
                      Qui puoi dichiarare solo D4 (OLD � implicito).

La parola che termina per Flg: indica se la stampante abbia una capacit�
particolare. Il valore assunto pu� essere True o False.

La parola .stateFlg: Indica se la stampante sia in grado di dire se sia
                     occupata, con stampa in corso, ecc...

La parola .cleanSeparateFlg: alcune stampanti permettono di pulire gli
                             ugelli separatamente. PEr queste stampanti
                             porre a True.

La parola .idFlg:            La Stylus Scan 2500 e probabilmente la
                             Stylus Scan 2000 non si identificano.
                             Per questi modelli porre a False.

La parola .exchangeFlg:      Alcune stampanti (Stylus Color 480/580) non
                             permettono ci� tramite pulsanti sulla stampante.
                             Dev'essere fatto tramite software. Per queste
                             stampanti porre il valore a True.

La parola .exchangeSeparateFlg: Le sopracitate stampanti hanno bisogno
                                di selezionare la cartuccia da sostituire.
                                Porre il valore a True.

Alcune stampanti, come la Stylus Photo 890, permettono anche di
sostituire la cartuccia via software. Se vuoi usare questa possibilit�
porre il flag .exchangeFlg a True e .exchangeSeparateFlg a False.

Le rimanenti voci che terminano con Nb:
- .passesNb:
- .ChoicesNb:
- .colorPassesNb:
- .colorChoicesNb:
sono per la procedura di allineamento. Le prime due indicano quanti
passi siano necessari, e quante scselte siano possibilit. Questi
valori possono essere visualizzati dal driver Windows o Mac OS forniti
da EPSON. Se la stampante non permette la regolazione della testina
del colore, porre il valore di .colorChoicesNb a 0.

L'ultima parola chiave .alignFunctionName � stata introdotta in modo
particolare per la Stylus Photo 820. Questa stampante non ha un codice
incorporato che stampi lo schema desiderato. Lo schema dev'essere
fornito da ttink/mtink. Per la Stylus Photo 820 inserire Pattern820,
per le altre porre il valore a "-".

Installare in file printer.desc:
--------------------------------

Ttink/Mtink si aspettano di trovare il file printer.desc in una delle
seguenti directory:

- /usr/lib/mtink
- /usr/local/lib/mtink
- /opt/mtink

Se il file printer.desc viene trovato, le directory successive non
vengono analizzate.

Se volessi modificare la descrizione per una delle stampanti,
estrai il dato con rdPrtDesc e modifica il valore per il file
desiderato, poi copia il file printer.desc cos� ottenuto in una
delle directory sopra indicate.
La descrizione nel file printer.desc ha la precedenza.

Compilare una nuova stampante in ttink/mtink:
---------------------------------------------

Genera il tuo file printer.desc nella directory .../mtink/utils
e chiama

rdPrtDesc -c > ../model.c

Il file per le stampani presenti e quella nuova viene generto automaticamente.
 
Torna alla directory mtink e chiama make e make install.
