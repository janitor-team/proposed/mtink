bldRes:

Dieses Programm liest die angegebene Ressourcen-Dateien und
erzeugt eine .c-Datei. Die Ausgabe erfolgt auf der Konsole.

Syntax: bldRes resource_datei_1 .....

Beispiel:

bldRes ../Ttink.en ../Ttink.de | sed 's/^\.En//' > ../mainSrc/tres.c
