#!/bin/sh
# check for the gimp plugin directory

getGimptoolDir()
{
   for dir in `echo $PATH | tr ':' ' '`
   do
      if [ -x $dir/gimptool ]
      then
         echo $dir
         break;
      fi
   done
}

# this work perfectly with gimp 1.0, 1.1 and 1.2.
# gimptool will alway fail but we can get the wanted path

getDir()
{
  sh -x $gimptoolDir/gimptool ---install-admin-bin t.sh --just-print --dry-run 2>&1 | tee -a /dev/tty| sed -n 's/.*plug[_]*in[_]*dir=\(.*\)/\1/p'
}

gimptoolDir=`getGimptoolDir`

getDir
