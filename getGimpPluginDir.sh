#!/bin/sh
# check for the gimp plugin directory

getGimptoolDir()
{
   for dir in `echo $PATH | tr ':' ' '`
   do
      if [ -x $dir/gimptool ]
      then
         echo $dir
         break;
      fi
      if [ -x $dir/gimptool-2.0 ]
      then
         echo $dir
         break;
      fi
   done
}

# this work perfectly with gimp 1.0, 1.1 and 1.2.
# gimptool will alway fail but we can get the wanted path

getDir()
{
  Res=`sh -x $gimptoolDir/gimptool ---install-admin-bin t.sh --just-print --dry-run 2>&1 | sed -n 's/.*plug[_]*in[_]*dir=\(.*\)/\1/p'`
  if [ "$Res" = "" ]
  then
     # we may have gimp 2.0
     Res=`$gimptoolDir/gimptool-2.0 --gimpplugindir 2>&1`
     if [ "$Res" = "" ]
     then
        Res=`$gimptoolDir/gimptool --gimpplugindir 2>&1`
     fi
  fi
  echo $Res/plug-ins
}

gimptoolDir=`getGimptoolDir`

getDir
