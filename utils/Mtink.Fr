! File Mtink.Fr, french resources.
! Fichier Mtink.Fr, resource fran�aises.
!
! Notes pout les traducteurs. Ce fichier correspond au format des resources
! pour X11.
!
! Les s�quences suivantes ont une signification sp�ciale:
! \    en fin de ligne (les espace et tabulateurs ne sont pas admis)
!      signifie que le texte continue sur la ligne suivante.
! \n   correspond a un retour chariot.
! \    (\ et espace). L'espace fait parti du texte. 
!      Les espace sont normalement �limin�s en d�but des textes.
! \t   correspond a un tabulateur.   
! !    en debut de ligne (les espaces en d�but de ligne sont admissibles) 
!      d�note un commentaire.
! 
! Si vous traduis� un fichier resource vers une autre langue, il est neccessaire
! de modifier le mot clef en fonction de la langue cibl�e.
! Example:
! La resource EpsonUtil*unknown est a traduire en fran�ais:
!
!   EpsonUtil*unknown: unknown
!   EpsonUtil.Fr*unknown: inconnus
! EpsonUtil est le nom de classe du programmme, .Fr doit etre inserr� a 
! apr�s le nom de classe. Si la traduction s'effectue du fran�ais vers
! l'italien, .Fr est � remplacer par .It.
!
! Le code pour l'identification de la langue est extrait des variables
! d'environement LANG, LC_ALL und LC_MESSAGE. Le premier charact�re est
! transform� en lettre capitale.
!
! J'ai, dans le but d'une meilleure lisibilit�, r�partit les textes
! sur plusieurs lignes, ce qui devrait rendre le formatage plus clair.

EpsonUtil.Fr*unknown:\
inconnus

! Le num�ro de version est corrig� lors de la g�n�ration des fichiers C.

EpsonUtil.Fr*title_LB.labelString:\
UTILITAIRES EPSON V 0.9.12

EpsonUtil.Fr*legend_LB.labelString:\
Quantit� d'encre

EpsonUtil.Fr*ok_PB.labelString:\
Quitter

EpsonUtil.Fr.mainWindow.ok_PB.tooltip:\
Au revoir.

EpsonUtil.Fr*pref_PB.labelString\
 Pr�f�rence

EpsonUtil.Fr*pref_PB.tooltip:\
- Browser,\n\
- Bulles d'aide,\n\
- Fichier de connexion\n\
- Imprimante\n\
- Mode de travail

EpsonUtil.Fr*about_PB.labelString:\
A propos

EpsonUtil.Fr*about_PB.tooltip:\
Licence et\n
cooperateurs

EpsonUtil.Fr*help_PB.labelString:\
Aide

EpsonUtil.Fr*help_PB.tooltip:\
Afficher l'aide avec un browser.

EpsonUtil.Fr*check_PB.labelString:\
Test\n\
buses

EpsonUtil.Fr*check_PB.tooltip:\
Ici, vous pouvez controller\n\
l'�tat des buses.

EpsonUtil.Fr*clean_PB.labelString:\
Nettoyage\n\
buses

EpsonUtil.Fr*clean_PB.tooltip:\
Le nettoyages des buses\n\
permet l'�limination\n\
des "rayures".

EpsonUtil.Fr*align_PB.labelString:\
Alignement\n

EpsonUtil.Fr*align_PB.tooltip:\
Attention !\n\
Ceci peu rendre l'imprimante inutilisable.

EpsonUtil.Fr*reset_PB.labelString:\
RAZ\n\
imprimante

EpsonUtil.Fr*reset_PB.tooltip:\
La remise a z�ro\n\
ne fonctionne pas\n\
pour toutes les imprimantes.

EpsonUtil.Fr*cartridge_PB.labelString:\
Changement\n\
cartouche

EpsonUtil.Fr*cartridge_PB.tooltip:\
Pour les imprimantes\n\
sans commutateurs.

EpsonUtil.Fr*addPrinterTxt:\
Autre Imprimante

EpsonUtil.Fr*printerState_LB.labelString:\
Status: 

EpsonUtil.Fr*state_LB.labelString:
- 

EpsonUtil.Fr*noPrinter*messageString:\
Probleme de communication\n\
Controllez l'imprimante pour:\n\
"manque de papier", "encre epuis�e"\n\
"imprimante hors service"\n\
\n\
Notez que certaines imprimantes ne\n\
sont accessibles que plusieurs secondes apr�s\n\
la mise sous tension.

EpsonUtil.Fr*noPrinter*dialogTitle:\
Erreur

EpsonUtil.Fr*cfg1_LB.labelString:\
Choix de l'imprimante:

EpsonUtil.Fr*cfg2Printer_PB.labelString:\
Choix de l'imprimante:

EpsonUtil.Fr*cfg2Printer_PB.tooltip:\
Une liste d'imprimante est propos�e.

EpsonUtil.Fr*cfg2Device_PB.labelString:\
Choix du port:

EpsonUtil.Fr*cfg2Device_PB.tooltip:
Malheureusement, la d�tection\nn'est pas automatique !

EpsonUtil.Fr*next_PB.labelString:\
Suivant

EpsonUtil.Fr*next:\
Suivant

EpsonUtil.Fr*previous_PB.labelString:\
pr�c�dent

EpsonUtil.Fr*previous:\
pr�c�dent

EpsonUtil.Fr*save:\
Enregistrer

EpsonUtil.Fr*cancel:\
Annuler

EpsonUtil.Fr*ok:\
OK

EpsonUtil.Fr*about:\
EPSON Utilities\n\
\n\
Version V 0.9.12\n\nCopyright: Jean-Jacques Sarton 2001\n\
\n\
Email: jj.sarton@t-online.de\n\
\n\
URL: http://xwtools.automatix.de\n\
\n\
Des portions de code ont ete tir�es du projet gimp-print\n\
Copyright 2000 Robert Krawitz (rlk@alum.mit.edu)\n\
\n\
Licence: GPL\n\
\n\
Mes remerciement a:\n\
Keith Amidon\n\
  camalot@picnicpark.org\n\
Ronny Budzinske\n\
Nicola Fabiano\n\   ivololeribar@yahoo.it\n\
Tokai Ferenc\n\
Karlheinz Guenster\n\
Gene Heskett\n\
   gene_heskett@iolinc.net\n\
Mogens J�ger\n\
   mogensjaeger@get2net.dk\n\
Till Kamppeter\n\
  (leader Foomatic/www.linuxprinting.org project)\n\
  http://www.linuxprinting.org/till\n\
Stefan Kraus\n\
   sjk@weserbergland.de\n\
   http://xwgui.automatix.de\n\
Rainer Krienke\n\
   krienke@uni-koblenz.de\n\
Sylvain Le-Gall\n\
   sylvain.le-gall@polytechnique.org\n\
Steven J. Mackenzie\n\
Raul Morales\n\
   raul.mh@telefonica.net\n\
   http://www.telefonica.net/web/ruten\n\
Simon Morlat\n\
Marc Riese\n\
   Marc-Riese@gmx.de\n\
Hikmet Salar\n\
   Salar@gmx.de\n\
Glen Stewart\n\
Daniel Tamm\n\
   daniel@tamm-tamm.de\n\
   http:/www.tamm-tamm.de\n   http:/www.expedit.org\n\
Robert Wachinger\n\
   nospam@robert-wachinger.de\n\
Klaus W�nschel\n\
   klaus.wuenschel@knittelsheim-computer.de\n\
   http:/www.knilse.de\n\
Alberto Zanoni.

EpsonUtil.Fr*ctTxt0:\
Alignement du chariot:\n\
\n\
Placez une feuille vierge dans l'imprimante et activez [Suivant]pour continuer ou [Annuler]

EpsonUtil.Fr*ctTxtC0:\
Alignement du chariot:\n\
\n\
Placez une feuille vierge dans l'imprimante,choisissez la t�te \
(noir ou couleur) et activez [Suivant] pour continuer ou [Annuler]

EpsonUtil.Fr*ctTxt1:\
Inspectez l�impression et choisissez la meilleure paire de lignes.\n\
Remettez la feuille dans l�imprimante et activez [Suivant] pour continuer.

EpsonUtil.Fr*ctTxt1_1:\
Echantillon #1\n\
Inspectez l�impression et choisissez la meilleure paire de lignes.\n\
Activez [Suivant] pour continuer.

EpsonUtil.Fr*ctTxt1_2:\
Echantillon #2\n\
Inspectez l�impression et choisissez la meilleure paire de lignes.\n\
Activez [Suivant] pour continuer.

EpsonUtil.Fr*ctTxt1_3:\
Echantillon #3\n\
Inspectez l�impression et choisissez la meilleure paire de lignes.\n\
Activez [Suivant] pour continuer.

EpsonUtil.Fr*ctTxt1C:\
Inspectez la feuille d'alignment, et determinez l'echantillon le plus "lisse".\n\
Cet echantillon est celui qui semble etre le moins "granulleux".\n\
Si vous ne trouvez pas d'echantillon "lisse" choisissez le meilleur echantillon\n\
et r�p�tez la procedure d'ajustage.\n\
Activez [Suivant] pour continuer.

EpsonUtil.Fr*ctTxtP:\
Attendez jusqu�� la fin de l�impression et activez [Suivant] pour continuer.

EpsonUtil.Fr*ctTxt5:\
Inspectez le resultat final afin de d�terminer si l'alignment est correct.\n\
\n\
Vous pouvez:\n\
\n\
  [Enregistrer] les r�sultats dans l'imprimante ou\n\
  [Annuler] l'enregistrement dans l'imprimante

EpsonUtil.Fr*exTxt0:\
Remplacement des cartouches:\n\
\nActivez :\n\
\n\
  [Suivant] pour continuer ou\n\
  [Annuler]

EpsonUtil.Fr*exTxt00:\
Remplacement des cartouches:\n\
\n\
Choisissez noir ou couleur\n\
Activez :\n\
\n\
  [Suivant] pour continuer ou\n\
  [Annuler]

EpsonUtil.Fr*exTxt1:\
Remplacement des cartouches:\n\
\n\
Le chariot est ammene a la position de remplacement.

EpsonUtil.Fr*exTxt2:\
Remplacement des cartouches:\n\
\n\
remplacez la cartouche et:\n\
\n\
  activez [Suivant]

EpsonUtil.Fr*exTxt3:\
Remplacement des cartouches:\n\
\n\
  le remplissage est en cours.

EpsonUtil.Fr*colors_RC*four_TB.labelString:\
4 couleurs

EpsonUtil.Fr*colors_RC*six_TB.labelString:\
6 Couleurs

EpsonUtil.Fr*head_RC*col_TB.labelString:\
Couleur

EpsonUtil.Fr*head_RC*bw_TB.labelString:\
Noir

EpsonUtil.Fr*noAccess*dialogTitle:\
Erreur

EpsonUtil.Fr.scrTxt_MW.head_RC.bw_TB.tooltip:\
L'operation sur la\n\
cartouche contenant\n\
l'encre noire.

EpsonUtil.Fr.scrTxt_MW.head_RC.col_TB.tooltip:\
L'operation sur la\n\
cartouche contenant\n\
les encres color�es.

EpsonUtil.Fr*noAccess*messageString:\
Pas de droit d'acc�s aux\n\
fichiers /dev/...\n\
\nPour les distribitions bas�e sur Debian\n\
l'utilisateur doit �tre membre du groupe lp.\n\
\nRem�diez a ceci. Consulter la documentation si\n\
necessaire.

EpsonUtil.Fr*fsb*dialogTitle: Mtink

EpsonUtil.Fr*fsb*CancelLabelString:\
Annuler

EpsonUtil.Fr*fsb*okLabelString:\
OK

EpsonUtil.Fr*fsb*applyLabelString:\
Filtre

EpsonUtil.Fr*fsb*filterLabelString:\
Filtre

EpsonUtil.Fr*fsb*fileListLabelString:\
Fichiers

EpsonUtil.Fr*fsb*dirListLabelString:\
Repertoires

EpsonUtil.Fr*browser_LB.labelString:\
Choisissez le Browser

! Attention un espace se trouve en fin de ligne

EpsonUtil.Fr*browser_PB.labelString:\
 ...\ 

EpsonUtil.Fr*browser_PB.tooltip:\
Choix du browser.

EpsonUtil.Fr*tooltip_TB.labelString:\
Bulles d'aide

EpsonUtil.Fr*tooltip_TB.tooltip:\
Bulles d'aide\n\
hors / en service

EpsonUtil.Fr*autodetect_TB.labelString:\
Autoriser la detection automatique

EpsonUtil.Fr*autodetect_TB.tooltip:\
La d�tection automatique de\n\
l'imprimante ne fonctionne\n\
pas sur tout les mod�les.\n\
Il est n�cessaire, pour certaine\n\
imprimantes de ne pas autoriser\n\
cette d�tection

EpsonUtil.Fr*save_PB.labelString:\
Enregistrer

EpsonUtil.Fr*save_PB.tooltip:\
Prise en compte des modifications.

EpsonUtil.Fr.ConfigureForm.cancel_PB.tooltip:\
Quiiter le mmasque sans\n\
prise en compte des modifications.

EpsonUtil.Fr*cancel_PB.labelString:\
Annuler

! Printer state
EpsonUtil.Fr*error:\
Erreur

EpsonUtil.Fr*printing:\
Impression

EpsonUtil.Fr*selfTest:\
Test

EpsonUtil.Fr*busy:\
Occup�

EpsonUtil.Fr*ok:\
OK

EpsonUtil.Fr*cleaning:\
Nettoyage

EpsonUtil.Fr*unknown:\
Inconnu
