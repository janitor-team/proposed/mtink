rdPrtDesc:
----------

This utilities allow the extraction of the printer description
datas need by ttink/mtink and print them in an human readable
form.

If you provide your own description, the description file
will be read and you can produce a new d files with all
datas which are relevants.

Syntax: rdPrtDesc -p|-c

-p rdPrtDes create the human readable file which will include
   all known printers.
   
-c rdPrtDes read the file printer.desc which must reside into
   the actual directory and generate a new c file which can
   be used for ttink/mtink.
   The known printer data and those from the description
   file are merged.


FILE printer.desc:
------------------

This file contain blocks which describe the printer capability.
A block look like this:

.PRINTER
   .name:                Stylus C20SX
   .colorsNb:            4
   .mainProt:            D4
   .stateFlg:            True
   .exchangeFlg:         False
   .exchangeSeparateFlg: False
   .cleanSeparateFlg:    True
   .resetProt:           D4
   .alignProt:           OLD D4
   .idFlg:               True
   .passesNb:            3
   .choicesNb:           15
   .colorPassesNb:       2
   .colorChoicesNb:      9
   .alignFunctionName:   -
.END


The word .PRINTER declare that a new description begins. The following
words up to the .END word, but not including it, describe the
capabilities for the printer.

Please don't begin a line with a dot '.', this character tell the
ttink/mtink as well as the help program rdPrtDesc that a description
line begins.

The .name: word must contain the name of the printer as returned by the
printer name query. 

The .colorsNb: tell ttink/mtink how many inks are used.

The designer ending with Prot: tell the programs which protocol
is to be used.

Newer printers know a new protocol which is named the D4 protocol.
This new protocol allows to communicate with the printer via
independant channels. This allows for example to query the remaining
ink quantity or the printer status while printing.
The D4 protocol includes also a set of commands, so that most of
the old commands has a replacement. Unfortunately not all printers
offer the full set of D4 commands.
Due to this, and also for compatibility to drivers which don't
know the D4 protocol, it is possible to tell the printer that the
D4 protocol must be disabled.

According to this the following combinations values are possible:

- OLD       The printer don' know anythings about D4.
- OLD D4    Use non D4 command, but send this via the D4 protocol
- OLD EXD4  Use the normal commands and set the printer into the
            compatibility mode.
- D4        The printer know a special D4 command, use this.

The word .mainProt:  declare the default mode.

The word .resetProt: declare the protocol type top use for the reset
                     command. D4 printers may all know this command
                     but not perform the operation as needed
                     (e.g. Stylus Color 980 or the Stylus Scan ...)

The word .alignProt: This command seem not to have a D4 counterpart
                     and the classic command will alltime be used.
                     Here you may declare only D4 (OLD is implied).

The word ending with Flg: tell if the printer has a particular
capability. The assigned value may be True or False.

The word .stateFlg:         Tell if the printer is able to return
                            if it is busy, working,...

The word .cleanSeparateFlg: a few printers allow to clean separately
                            the nozzle. For those printers declare
                            True.

The word .idFlg:            The Stylus Scan 2500 and probably the
                            Stylus Scan 2000 don't return an identification.
                            For such model this flag must be set to False.
                             

The word .exchangeFlg:         A few printers (Stylus Color 480/580) don't
                               allows to do this via button on the printer.
                               This must be done by software. For such
                               printers set the value to True.

The word .exchangeSeparateFlg: Both above mentionned printers need a selection
                               of the cartridge which is to be exchanged.
                               Set the value to True.

Some printers as the Stylus Photo 890 allow also to exchange the ink
cartridge via software. If you want to use this possibility set the
flag .exchangeFlg to True and .exchangeSeparateFlg to False.

The remaining designer ending with Nb:
- .passesNb:
- .ChoicesNb:
- .colorPassesNb:
- .colorChoicesNb:
are for the aligment procedure. The first two entries tell how many
passes are needed, and how many choices are allowed. These values can
be seen from the windows or Mac OS driver delivered by EPSON.
If the printer don't allow an adjustement for the color head, set
the value for .colorChoicesNb to 0.

The last keyword .alignFunctionName was introduced specially for
the Stylus Photo 820. This printer don't have a buildin code which
print the desired pattern. The pattern must be supplied by ttink/mtink.
For the Stylus Photo 820, enter Pattern820, for others set the value to
"-".

Istalling a printer.desc file:
------------------------------

Ttink/Mtink expect to find the printer.desc file in one of the
following directories:

- /usr/lib/mtink
- /usr/local/lib/mtink
- /opt/mtink

If the printer.desc file is found, the remaining directories are
not scanned.

If you want to modify the description for one of the given printers,
extract the data with rdPrtDesc and modify the value for the
wanted file, then copy the resulting printer.desc file to one
of the above directory.
The description find into the printer.desc file prevail.

Compiling a new printer into ttink/mtink:
-----------------------------------------

generate your printer.desc file under the directory .../mtink/utils
and call 

rdPrtDesc -c > ../model.c

The file for actual printers and the new one is then automatically generated.
 
return then to the mtink directory and call make and make install.
