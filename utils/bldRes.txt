bldRes:

This program read the given resource files and create a c-file
which can be included in the main program.
if the file name contain utf8, the resource name will be modified
from '.De.*' to .De8.*' 

Syntax: bldRes resource_file_1 .....

The output is done on the console.

If you whish to create the resource file, rtedirect the output
to the wanted file.


example:

#bldRes Ttink.en Ttink.de > ../mainSrc/tres.c
bldRes Ttink.?? Ttink.??.utf8 > ../mainSrc/tres.c
