rdPrtDesc:
----------

Dieses Hilfsprogramm erlaubt es, die Druckerbeschreibungen zu
extrahieren und diese in einer menschenlesbare Form zu pr�sentieren.

Wenn Sie Ihre eigene Beschreibungsdaten zur Verf�gung stellen,
wird die Datei printer.desc von ttink/mtink gelesen und deren
Inhalt zur Menge der bekannten Drucker hinzugef�gt.


Syntax: rdPrtDesc -p|-c

-p rdPrtDes: Eine f�r Menschen lesbare Version der Druckerdaten f�r alle
   bekannten Drucker erzeugen.
   
-c rdPrtDes: Die Datei printer.desc lesen und die entsprechende .c-Datei
   erzeugen. Die Datei printer.desc muss im aktuellen Verzeichnis sein.
   Die Daten f�r den bereits bekannten Druckern werden hinzugef�gt.
   Die Ausgabe erfolgt auf das Terminal, kann aber in eine Datei
   umgeleitet werden.


Datei printer.desc:
-------------------

Diese Datei beinhaltet Abschnitte, die den einzelnen Drucker beschreiben.
Ein Block sieht so aus:

.PRINTER
   .name:                Stylus C20SX
   .colorsNb:            4
   .mainProt:            D4
   .stateFlg:            True
   .exchangeFlg:         False
   .exchangeSeparateFlg: False
   .cleanSeparateFlg:    True
   .resetProt:           D4
   .alignProt:           OLD D4
   .idFlg:               True
   .passesNb:            3
   .choicesNb:           15
   .colorPassesNb:       2
   .colorChoicesNb:      9
   .alignFunctionName:   -
.END


Das Wort .PRINTER leitet einen neuen Datensatz ein. Die nachstehenden W�rter
bis zum Wort .END sind die eigentliche Beschreibung.

Bitte die Zeilen nicht mit dem Zeichen '.' beginnen, denn diese Zeichen
bedeutet f�r mtink/ttink, dass eine g�ltige Beschreibungszeile schon
vorhanden ist. Zeilen, die nicht mit einem '.' beginnen, werden als
Kommentarzeilen interpretiert. Leerzeichen am Beginn einer Zeile werden
�berlesen, so dass "    .xxx" nicht als Kommentar verarbeitet wird.

"- .name:" muss die Bezeichnung des Druckers beinhalten, und zwar genau so, wie
es der Drucker nach einer Abfrage zur�ckliefert. 

".colorsNb:" gibt an wieviel Farben vorhanden sind.

W�rter die mit "Prot:" enden, geben das Protokoll an, das f�r die jeweilige
Funktion zu verwenden ist.

Neuere Druckern beherrschen ein neuen Protokoll (D4) welches es erlaubt,
mit dem Drucker �ber unabh�ngige Kan�le zu kommunizieren. Dies erlaubt es
beispielsweise, die Resttintenmenge abzufragen, obwohl der Drucker
noch mit Drucken besch�ftigt ist.

Das D4-Protokoll beinhaltet ebenfalls einen Satz Kommandos, die
zum Teil die normalen Kommandos (REMOTE Kommandos) ersetzen k�nnen.
Ungl�cklicherweise unterscheiden sich die Drucker erheblich bez�glich
der Menge der implementierten D4-Kommandos.

Dementsprechend is es m�glich anzugeben, welcher Kommandotyp zu verwenden ist,
um die Verwendung des D4-Protokoll (Verwendng mit �lteren Druckern) zu
unterbinden.


Nachstehende Eintr�ge sind m�glich:

- OLD       Der Drucker unterst�tzt nicht D4.
- OLD D4    Nicht D4-Kommandos verwenden aber in das D4-Protokoll einbetten.
- OLD EXD4  Alte Kommandos verwenden und Drucker im Kompatibilit�tsmodus
            betreiben (das D4-Protokoll wird abgeschaltet).
- D4        Der Drucker kennt D4 Kommandos, das auch verwenden.

".mainProt:"  Default Modus festlegen.

".resetProt:" Kommando f�r R�cksetzen deklarieren. Manche Drucker besitzen auch
              eine D4-Version die allerdings nicht zum gew�nschten Ergebnis
              f�hrt (Stylus Color 980 oder die Stylus Scan ... Serie).

".alignProt:" Dieses Kommando hat offenbar keine D4 Entsprechung. Das
              klassische Kommando wird stattdessen immer verwendet.

W�rter die mit "Flg" enden, geben an, ob der Drucker eine bestimmte F�higkeit
besitzt. Die zugewiesenen Werte sind True (Wahr) oder False (Falsch).

".stateFlg:"  Gibt an, ob der Drucker Zustandsangaben (Druck im Gang,...)
              zur�ckgeben kann.

".cleanSeparateFlg:" Einige wenige Drucker erlauben es, die D�senreinigung
                     geziehlt vorzunehmen (nur Schwarz/nur Farbe). F�r
                     solche Druckern ist "True" anzugeben.

".idFlg:"      Der Stylus Scan 2500, und wahrscheinlich auch der Stylus Scan
               2000, geben keine Identifizierung zur�ck. F�r solche Modelle ist
               "False" anzugeben.

".exchangeFlg:" Wenige Drucker, wie die Stylus Color 480/580, besitzen keine
                Bedienelemente, so dass das Auswechseln der Tintenpatrone
                eine Softwarel�sung erfordet. In diesem Fall "True" angeben.

".exchangeSeparateFlg:" Beide zuvor genannten Drucker ben�tigen eine
                        Angabe bez�glich der Patronne, die auszuwechseln ist.
                        Den Wert auf "True" setzen.

Manche Drucker, wie der Stylus Photo 890, erlauben es, das Auswechseln der
Patrone softwaregesteuert vorzunehmen. Wenn Sie w�nschen, diese M�glichkeit zu
verwenden, modifizieren Sie den Wert f�r  ".exchangeFlg:" von "False" auf
"True".

Die mit "Nb:" vesehenen W�rter:

".passesNb:"
".colorPassesNb:"
".colorPassesNb:"
".colorChoicesNb:"

beschreiben die D�sen-Ausrichtung. Die beiden ersten W�rter (Schwarz) geben
an, wieviele Durchg�nge notwendig sind und wieviele Antwortm�glichkeiten
vorhanden sind. Die zwei n�chsten Eintragungen betreffen die Ausrichtung
bez�glich der Farbd�sen.

Die m�glichen Werte k�nnen ermittelt werden indem der EPSON-Statusmonitor
f�r Windows oder Mac OS bem�ht wird. Wenn eine Ausrichtung der Farbd�sen nicht
vorhanden ist, muss der Wert f�r ".colorChoicesNb" 0 sein.

Das letzte Wort ".alignFunctionName" musste wegen dem Stylus Photo 820
eingef�hrt werden. Dieser Drucker besitzt keinen internen Code f�r das
Ausrichten der K�pfe, so dass der Statusmonitor diesen zur Verf�gung stellen
muss. Ausser f�r oben genannte Drucker (Wert = Pattern820) ist ein "-"
anzugeben.

Achtung: Der von Mtink abgesetze Code beinhaltet Steuersequenzen, die nur vom
Stylus Photo 820 verstanden werden. 


Datei printer.desc installieren:
--------------------------------

Ttink/Mtink erwarten die Datei printer.desc in einem der folgenden
Ordner:

- /usr/lib/mtink
- /usr/local/lib/mtink
- /opt/mtink

Wenn die Datei printer.desc gefunden wurde, werden die �brige Ordner 
nicht mehr untersucht.

Wenn Sie die Beschreibung Ihres Druckers �ndern m�chten, k�nnen Sie mit
"rdPrtDesc" die Daten f�r die bekannten Druckern extrahieren und
modifizieren. Nachdem diese Datei in eines der oben genannten Verzeichnisse
kopiert wurde, werden beim n�chsten Aufruf von ttink/mtink den ver�nderten
Daten der Vorrang gegen�ber den internen Daten gegeben.


Daten in einen neuen Drucker in ttink/mtink einbinden:
------------------------------------------------------

Die Datei printer.desc im Verzeichniss .../mtink/utils generieren,
dann erg�nzen und die c-Datei erzeugen mit:

rdPrtDesc -c > ../model.c

Die Daten f�r die alten und neuen Drucker sind nun in der Datei
../model.c.

Nach der R�ckkehr zum Hauptverzeichniss "make;make install" aufrufen.
