!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Svensk resursfil f�r ttink.
! Swedish recourse file for ttink.
! This is for translators.
!
! This file is built like a normal X-resources file
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

! !             is a commment character if it appears at the begin of a
!               line.
!
! .syntax1:     is ein key word.
! .En.syntax1:  is the key for a localized text.
!               both characters (in this case En) are taken from the
!               environment variablea LANG, LC_ALL. The first letter
!               will be set to upper case and the second remain normal.
!               If you translate this file, please replace .En. with
!               the correct value.
!
! \             At the  end of a line (following spaces and  tabulators are
!               not allowed) show that the actual line will be continued.
!
! \     text    allow to indent the text. This will only be required after
!               the key word. If no \ is present, spaces are removed.
!
! \n            This is a carriage return. Following text will be
!               printed in the next line.
!
!               Please note that spaces at the end of a line will not
!               be removed.

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

.Sv.syntax1:\
 Argumenter f�r 

.Sv.syntax2:\
  Obligatorisk:\n\
    -d apparatfil --device apparatfil\n\
  Optional:\n\
    -m namn        --model namn eller nummer\n\
        utan detta uppt�ckas skrivaren automatiskt\n\
    -D  --D4              Anv�nd alltid D4-protokollet\n\
  Uppdrag (bara ett uppdrag kan ges):\n\
    -r --reset            S�tt tillbaka skrivaren\n\
    -c --clean            Reng�r munstycken\n\
    -n --nozzle-check     Testa munstycken\n\
    -s --status           (standarduppdrag)\n\
    -a --align-head       r�t upp huvudet\n\
    -e --exchange-ink     utbyt patronen (inte alla skrivaren)\n\
    -i --identity         skriv ut skrivarens identitet\n\
  Information:\n\
    -v --version          skriv ut versionsnummer\n\
    -l --list-printer     skriv ut lista av k�nda skrivaren\n\
  Andra:\n\
    -L                    debug ouput f�r D4-protokollet p� stderr\n\
    -u                    utskrift med UTF-8-kod\n

.Sv.syntaxM:\
  Optional:\n\
    -D  --D4              Anv�nd alltid D4-protokollet\n\
  Uppdrag (bara ett uppdrag kan ges):\n\
    -r --reset            S�tt tillbaka skrivaren\n\
    -c --clean            Reng�r munstycken\n\
    -n --nozzle-check     Testa munstycken\n\
    -s --status           (standarduppdrag)\n\
    -a --align-head       r�t upp huvudet\n\
    -e --exchange-ink     utbyt patronen (inte alla skrivaren)\n\
    -i --identity         skriv ut skrivarens identitet\n\
  Information:\n\
    -v --version          skriv ut versionsnummer\n\
    -l --list-printer     skriv ut lista av k�nda skrivaren\n\
  Andra:\n\
    -L                    debug ouput f�r D4-protokollet p� stderr\n\
    -u                    utskrift med UTF-8-kod\n

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Error and warning messages

.Sv.noAccess:\
Ingen tillg�ng till apparatfilen eller ingen skrivare �r ansluten.

.Sv.noDetected:\
Kan inte best�mma skrivarens modell.

.Sv.unknownModel:\
Ok�nd skrivare

.Sv.noOPen:\
Kan inte �ppna apparatfilen.


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Strings for queries and so on

.Sv.continue:\
Forts�tta (ja/nej) [nej] ? : 

.Sv.yesorno: jn
.Sv.saveCancel: sa

.Sv.blackQ: svart
.Sv.colorQ: f�rg

.Sv.followingPrintersFound:\
Hittade f�ljande skrivare:

.Sv.ChoosePrinter:\
V�lj skrivaren 

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Status

.Sv.black:    Svart
.Sv.cyan:     Cyan
.Sv.magenta:  Magenta
.Sv.yellow:   Gul
.Sv.lcyan:    Ljuscyan
.Sv.lmagenta: Ljusmagenta
.Sv.lblack:   Foto/matt svart
.Sv.blue:     Bl�
.Sv.red:      R�d
.Sv.dyellow:  M�rkgul
.Sv.gloss:    Glansoptimerare

.Sv.printerState: skrivarens status
.Sv.unknown:      ok�nd
.Sv.selfTest:     sj�lvtest
.Sv.busy:         upptagen
.Sv.printing:     skriver ut
.Sv.cleaning:     reningen p�g�r
.Sv.ok:           Ok

.Sv.error:                Fel
.Sv.interfaceNotSelected: Gr�nssnittet �r inte valt
.Sv.paperJamError:        Pappersstockningsfel
.Sv.inkOutError:          Bl�cket �r slut
.Sv.paperOutError:        Pappret �r slut

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Head aligment

.Sv.alignWarning:\
Varning !\nDetta kunde f�rst�ra skrivaren.

.Sv.alignBlackOrColor:\
R�ta upp det svarta eller det f�rgade huvudet?

.Sv*firstHeadAlign:\
V�nligen s�tt in en pappersark i din skrivare.

.Sv.nextHeadAlign:\
V�nligen s�tt in igen arken i skrivaren f�r att fors�tta processen.

.Sv.lastHeadAlign:\
V�nligen granska den sista utskriften mycket noggrant f�r\n\
att tillf�rs�kra dig att skrivaren �r inst�lld bra.\n\n\
Du kan nu: spara resultatet i skrivaren eller\n\
avbryta utan att spara resultatet\n(spara/avbryta) [avbryta] : 

.Sv.choosePattern:\
V�nligen granska utskriften och v�lj paret med\n\
dom rakaste linjer fr�n det sista m�nstret.\n\
S�tt arken i skrivaren igen.

.Sv.chooseCPattern:\
Granska arken med m�nstren och fastst�ll\n\
m�nstret som �r j�mnast.\n\
Om du inte kan hitta ett j�mnt m�nster\n\
v�lj nummret av det b�sta m�nstret,\n\
och repetera processen.\n

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Exchange of cartridge

.Sv.functionNA:\
Denna funktion �r inte tillg�nglig f�r din skrivare.

.Sv.askDoExchange:\
Vill du utbyta en patron (ja/nej) [nej] ? : 

.Sv.exchangeBlackOrColor: V�lj patronen:

.Sv.adviseMoveCartridge:\
Skrivhuvudet flyttas nu till utbytespositionen.

.Sv.adviseExchangeCartridge:\
Du kan nu byta patronen.

.Sv.askExDone:\
Utbyte klart (ja/nej) [nej] ? : 

.Sv.adviseFillCartridge:\
P�fyllning av bl�cket startas nu.

.Sv.adviseExchangeDone:\
Utbyte av patronen �r klar.

.Sv.communicationError:\
L�s-/skrivfel p� skrivaren!

.Sv.exchangeError:\
Skrivaren svarar med fel vid beg�ran av patronbyte!
