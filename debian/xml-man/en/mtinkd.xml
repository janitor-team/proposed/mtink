<?xml version="1.0" encoding="ISO-8859-1"?>
<!DOCTYPE refentry PUBLIC "-//OASIS//DTD DocBook XML V4.4//EN" 
"/usr/share/xml/docbook/schema/dtd/4.4/docbookx.dtd" [
  <!ENTITY debian "Debian GNU/Linux">
  <!ENTITY dhprg "<command>mtinkd</command>">
]>

<!--**********************************************************************-->
<!-- Mtinkd manpage                                                       -->
<!--                                                                      -->
<!-- Copyright (C) 2002-2006 Sylvain Le Gall <gildor@debian.org>          -->
<!--                                                                      -->
<!-- This library is free software; you can redistribute it and/or        -->
<!-- modify it under the terms of the GNU Lesser General Public           -->
<!-- License as published by the Free Software Foundation; either         -->
<!-- version 2.1 of the License, or (at your option) any later version;   -->
<!-- with the OCaml static compilation exception.                         -->
<!--                                                                      -->
<!-- This library is distributed in the hope that it will be useful,      -->
<!-- but WITHOUT ANY WARRANTY; without even the implied warranty of       -->
<!-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU    -->
<!-- Lesser General Public License for more details.                      -->
<!--                                                                      -->
<!-- You should have received a copy of the GNU Lesser General Public     -->
<!-- License along with this library; if not, write to the Free Software  -->
<!-- Foundation, Inc., 51 Franklin St, Fifth Floor, Boston,               -->
<!-- MA 02110-1301, USA.                                                  -->
<!--                                                                      -->
<!-- Contact: gildor@debian.org                                           -->
<!--**********************************************************************-->

<refentry>
  
  <xi:include xmlns:xi="http://www.w3.org/2001/XInclude" href="refentryinfo.xml"/>

  <refmeta>
    <refentrytitle>MTINKD</refentrytitle>
    <manvolnum>8</manvolnum>
  </refmeta>

  <refnamediv>
    <refname>mtinkd</refname>
    
    <refpurpose>daemon to control your EPSON printer</refpurpose>
  </refnamediv>
  
  <refsynopsisdiv>
    <cmdsynopsis>
      &dhprg; 
      <arg>-dev <replaceable>device_file</replaceable></arg>
      <arg>-name <replaceable>model</replaceable></arg>
      <arg>-usbbase <replaceable>pattern_device_file</replaceable></arg>
      <arg>-nodaemaon</arg>
      <arg>-model <replaceable>model_name</replaceable></arg>
      <arg>-debug</arg>
      <group>
        <arg>start</arg>
        <arg>stop</arg>
        <arg>status</arg>
      </group>
    </cmdsynopsis>
  </refsynopsisdiv>
  
  <refsect1>
    <title>DESCRIPTION</title>
    
    <para>This manual page documents briefly the &dhprg; command.</para>
    
    <para>This manual page was written for the &debian; distribution because
      the original program does not have a manual page.</para>
    
    <para>&dhprg; is a status monitor for EPSON ink jet printer</para>
    
    <para>It permits one to watch the remaining ink while printing.</para>
    
    <para>As it keeps <filename>/dev/lp*</filename> open, you have to change
      your printing parameter to link the printer to
      <filename>/var/run/mtink/printer</filename></para>
  </refsect1>

  <refsect1>
    <title>OPTIONS</title>
    
    <para>These programs follow the usual GNU command line syntax, with long
      options starting with two dashes (`-').  A summary of options is included
      below.</para>
    
    <para>Options for &dhprg;</para>
    
    <variablelist>
      <varlistentry>
        <term>
          <option>-dev <replaceable>device_file</replaceable></option>
        </term>
        <listitem>
          <para>This option is mandatory and is also used in order to get the
            status or to stop a running server.  Device file is the name of the
            character device on which the printer is attached
            (<filename>/dev/lp0</filename>, <filename>/dev/usb/lp0</filename>,
            <filename>/dev/ecpp0</filename>).</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term><option>-nodaemon</option></term>
        <listitem>
          <para>This option is for test purpose, without this option mtink
            start in the background.</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term><option>-name <replaceable>model</replaceable></option>
        </term>
        <listitem>
          <para>If the printer don't return it identity you may tell which
            printer is attached. The name is derived from the official name and
            space characters are to be replaced by the "_" character. This
            will be the case for the Stylus Scan 2500 (model name is Scan_2500).</para>
          <para> This option may also be useful for multifunction device as
            the Stylus Photo 895 under Linux. If the printer is powered off and
            then on, the usb kernel will not detect the printer properly and
            mtinkd must be at least stopped and started again.</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term><option>-model <replaceable>model_name</replaceable></option>
        </term>
        <listitem>
          <para>The name of the named pipe will be set according to this name.
            If the -model option is not given, mtinkd will terminate if the
            identification returned don't comply with passed name.</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term><option>-usbbase</option>
        </term>
        <listitem>
          <para>This option requires a special system call which is only
            implemented into newer kernels.</para>
          <para>The -type name option is required !</para>
          <para> path and device file base may be /usr/usblp or /usr/usb/lp.
            All device files matching whith the passed name will be scanned
            for the wanted printer, the device file will then be used later.
          </para>
          <para>If the kernel don't support the special call, scanning will
            fail and if the -dev device_file option was given, the given device
            file name will then be used.</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term><option>start</option></term>
        <listitem>
          <para>The server will be started. start may be ommited.</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term><option>stop</option>
        </term>
        <listitem>
          <para>An running server will be stopped.</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term><option>status</option>
        </term>
        <listitem>
          <para>This allow to ask if a server is running.</para>
        </listitem>
      </varlistentry>
    </variablelist>
  
  </refsect1>

  <xi:include xmlns:xi="http://www.w3.org/2001/XInclude" href="license.xml"/>

  <refsect1>
  <title>SEE ALSO</title>
  
  <para><filename>/usr/share/doc/mtink/html/index.html</filename></para>
  </refsect1>
</refentry>
