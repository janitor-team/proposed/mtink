/* file model.h
 *
 * List of know printers and flags
 * device and printer choice
 *
 */
/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef MODEL_H

#define MODEL_H

#if defined(__cplusplus) || defined(c_plusplus)
extern "C" {
#endif

#ifndef NULL
#define NULL ((void *)0)
#endif
#ifndef True
#define True 1
#endif
#ifndef False
#define False 0
#endif

#define PALL      7
#define NO_PROT   0
#define PROT_OLD  1
#define PROT_EXIT 2
#define PROT_NEW  (PROT_OLD|PROT_EXIT)
#define PROT_D4   4


#define DEV_CLOSE      8
#define STAY_RESIDENT 16


#define TEST_DEV       1
#define GET_ID         2
#define GET_STAT       3
#define GET_IQ         4
#define RESET_PRT      5
#define CHECK_NOZZLE   6
#define CLEAN_NOZZLE   7
#define CLEAN_NOZZLE_C 8
#define CLEAN_NOZZLE_B 9
#define WAIT_STAT_04   10
#define WAIT_STAT_01   11
#define EXCHANGE_ALL   12
#define EXCHANGE_B     13
#define EXCHANGE_C     14
#define EXCHANGE_NEXT  15
#define ALIGN_HEAD     16
#define ALIGN_HEAD_C   17
#define OPEN_DEV       18
#define RELEASE        254
#define TERMINATE      255

typedef struct configData_s
{
   char *name;
   int   colors;
   int   prot;                  /* protocol main entry */
   int   state;                 /* Bool capability */
   int   exchange;              /* Bool capability */
   int   exchangeSeparate;      /* Bool capability */
   int   cleanSeparate;         /* Bool capability */
   int   reset;                 /* Protocol for reset */
   int   align;                 /* Protocol for alignment */
   int   id;                    /* Bool capability */
   /* taken from gimp-print escputil */
   int   passes;
   int   choices;
   int   color_passes;
   int   color_choices;  
   int   checkNeedReset;        /* Photo 890/1290 have a firmware bug so */
                                /* we must send a reset after the check  */
   char *alignFunction;         /* the Photo 890 require external code */
   char *dev;
} configData_t;

extern configData_t defaultConfigData[];
extern int defaultConfigDataSize;


#if defined(__cplusplus) || defined(c_plusplus)
}
#endif

#endif

