/* file rdRes.h
 *
 * Event handler for wheelmouse
 */
/*
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef WHEELMOUSE_H

#define RDRES_H

#if defined(__cplusplus) || defined(c_plusplus)
extern "C" {
#endif

extern char *searchString(char *lang, char *key);
extern int   askYn(char *lang, char *yn);
extern int   askNo(int first, int last);
extern void  printText(char *fmt, char *lang, char *key);
extern void  fprintText(FILE *out, char *fmt, char *lang, char *key);
extern int   askForChoice(char *lang, char *choice[], int no);
extern void  initTerm(void);
extern void  resetTerm(void);
extern int  initResource(void);

#if defined(__cplusplus) || defined(c_plusplus)
}
#endif

#endif
