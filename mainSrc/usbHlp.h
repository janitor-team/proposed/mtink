/* usb.h */
#ifdef MACOS
#include <usb.h>
extern int     mOpen(const char *name, int flags);
extern ssize_t mWrite(int fd, char *buf, size_t count);
extern ssize_t mRead(int fd, void *buf, size_t count);
extern int     mClose(int fd);
extern int     usbInit(void);

extern int     unlinkFiles(void);
extern int     createFiles(void);
extern void    showModels(void);
extern char   *getPrinterName(int dn);

typedef struct
{
  int                open;
  char              *devname;
  char              *prtname;
  int                vendor;
  int                product;
  int                bulk_in_ep;
  int                bulk_out_ep;
  int                interface_nr;
  usb_dev_handle    *libusb_handle;
  struct usb_device *libusb_device;
} device_list_type;

extern device_list_type devices[];

#define USB_PRT_DIR "/var/mtink"
#define USB_PRT_NAME_PREFIX USB_PRT_DIR"/usb:"
#endif
