/*
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef D4LIB_H

#define D4LIB_H

#if defined(__cplusplus) || defined(c_plusplus)
extern "C" {
#endif

extern int debugD4;   /* allow printout of debug informations */

typedef unsigned char uc;
extern int EnterIEEE(int fd);
extern int Init(int fd);
extern int Exit(int fd);
extern int GetSocketID(int fd, char *serviceName);
extern int OpenChannel(int fd, uc sockId, int *sndSz, int *rcvSz);
extern int CloseChannel(int fd, uc socketID);
extern int CreditRequest(int fd, uc socketID);
extern int Credit(int fd, uc socketID, int credit);

/* convenience function */
extern int askForCredit(int fd, uc socketID, int *sndSz, int *rcvSz);
extern int writeData(int fd, uc socketID, uc *buf, int len, int eoj);
extern int readData(int fd, uc socketID, uc *buf, int len);

extern int d4WrTimeout;
extern int d4RdTimeout;
extern int ppid;

#if D4_DEBUG
#define DEBUG 1
#endif
#if 0
#if DO_SO_LIB
#define open  mOpen
#define close mClose
#define read  mRead
#define write mWrite

extern int (*mOpen)(char*,int);
extern int (*mClose)(int);
extern int (*mRead)(int,char*,int);
extern int (*mWrite)(int,char*,int);
#endif
#endif

#if defined(__cplusplus) || defined(c_plusplus)
}
#endif

#endif
