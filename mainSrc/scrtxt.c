/*
 * File srcTxt.c
 *
 */
/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */


#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>
#include <strings.h>
#include <signal.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>

#include <X11/Intrinsic.h>
#include <X11/StringDefs.h>
#include <X11/Xlib.h>
#include <Xm/Xm.h>
#include <Xm/MwmUtil.h>
#include <X11/Core.h>
#include <X11/Shell.h>
#include <Xm/PanedW.h>
#include <Xm/MainW.h>
#include <Xm/Form.h>
#include <Xm/ScrollBar.h>
#include <Xm/ScrolledW.h>
#include <Xm/List.h>
#include <Xm/Separator.h>
#include <Xm/RowColumn.h>
#include <Xm/TextF.h>
#include <Xm/Text.h>
#include <Xm/RowColumn.h>
#include <Xm/ToggleB.h>
#include <Xm/PushB.h>
#include <Xm/Label.h>
#include <Xm/MessageB.h>
#include <Xm/MwmUtil.h>
#include <Xm/Scale.h>

#include "mtink.h"
#include "wheelmouse.h"

static Widget scrTxtNext_PB;
static Widget scrTxtPrevious_PB;
static Widget title_LB;
static Widget scrTxt_TF;
static Widget scrTxt_MW;
static Widget bw_TB;
static Widget col_TB;
static Widget head_RC;
static Widget separator;

static int loop;

static void scrTxtNext_CB(Widget w, XtPointer clientData, XtPointer callData);
void scrTxtPrevious_CB(Widget w, XtPointer clientData, XtPointer callData);

/*******************************************************************/
/* Function createScrTxt_MW()                                      */
/*                                                                 */
/* Create a windows with a scrolled text and push buttons          */
/*                                                                 */  
/* Input: Widget parent not used                                   */
/*        int   *next   not used                                   */
/*                                                                 */
/* return: -                                                       */
/*                                                                 */
/*******************************************************************/

Widget createScrTxt_MW(Widget parent, int *next)
{
   Arg         args[20];
   int         n = 0;

   scrTxt_MW = XtVaCreateWidget("scrTxt_MW",
                                 xmFormWidgetClass,
                                 mainForm,
                                 XmNmarginHeight,     0,
                                 XmNmarginWidth,      0,
                                 XmNresizePolicy,     XmRESIZE_GROW,
                                 XmNresizable,        True,
                                 XmNleftAttachment,   XmATTACH_FORM,
                                 XmNrightAttachment,  XmATTACH_FORM,
                                 XmNtopAttachment,    XmATTACH_FORM,
                                 XmNbottomAttachment, XmATTACH_FORM,
                                 NULL);

   if (scrTxt_MW == NULL)
   {
      return NULL;
   }
   XtManageChild(scrTxt_MW);

   title_LB = XtVaCreateWidget("title_LB",
                             xmLabelWidgetClass,
                             scrTxt_MW,
                             XmNleftAttachment,   XmATTACH_FORM,
                             XmNleftOffset,       5,
                             XmNrightAttachment,  XmATTACH_FORM,
                             XmNrightOffset,      5,
                             XmNtopAttachment,    XmATTACH_FORM,
                             XmNtopOffset,        10,
                             XmNalignment,        XmALIGNMENT_CENTER,
                             NULL);
    
   XtManageChild(title_LB);


   /* create scrolled text field  */
   
   n = 0;
   XtSetArg(args[n], XmNtopAttachment,         XmATTACH_WIDGET); n++;
   XtSetArg(args[n], XmNtopOffset,             5); n++;
   XtSetArg(args[n], XmNtopWidget,             title_LB); n++;
   XtSetArg(args[n], XmNleftAttachment,        XmATTACH_FORM); n++;
   XtSetArg(args[n], XmNleftOffset,            5); n++;
   XtSetArg(args[n], XmNrightAttachment,       XmATTACH_FORM); n++;
   XtSetArg(args[n], XmNrightOffset,           5); n++;
   XtSetArg(args[n], XmNcursorPositionVisible, False); n++;
   XtSetArg(args[n], XmNeditable,              False); n++;
   XtSetArg(args[n], XmNeditMode,              XmMULTI_LINE_EDIT); n++;
   XtSetArg(args[n], XmNwordWrap,              True); n++;
   XtSetArg(args[n], XmNscrollHorizontal,      False);n++;
   XtSetArg(args[n], XmNhighlightOnEnter, False); n++;
   scrTxt_TF = XmCreateScrolledText(scrTxt_MW, "scrTxt_TF",args,n);
   XtManageChild(scrTxt_TF);


   /* create toggle buttons for the capabilities  */
   
   n = 0;
   XtSetArg(args[n], XmNleftAttachment,  XmATTACH_FORM); n++;
   XtSetArg(args[n], XmNleftOffset,      5);             n++;
   XtSetArg(args[n], XmNradioAlwaysOne,  True);          n++;
   XtSetArg(args[n], XmNradioBehavior,   True);          n++;
   XtSetArg(args[n], XmNradioBehavior,   True);          n++;
   XtSetArg(args[n], XmNpacking,         XmPACK_COLUMN); n++;
   XtSetArg(args[n], XmNnumColumns,      2);             n++;
   head_RC = XmCreateRowColumn(scrTxt_MW, "head_RC",args,n);
   XtManageChild(head_RC);

#ifndef XmSET
#define XmSET True
#endif

   bw_TB = XtVaCreateWidget("bw_TB",
                            xmToggleButtonWidgetClass,
                            head_RC,
                            XmNset, XmSET,
                            NULL); 
   XtManageChild(bw_TB);

   col_TB = XtVaCreateWidget("col_TB",
                            xmToggleButtonWidgetClass,
                            head_RC,
                            NULL); 
   XtManageChild(col_TB);

   /* create Pushbuttons */
   scrTxtPrevious_PB = XtVaCreateWidget("previous_PB",
                            xmPushButtonWidgetClass,
                            scrTxt_MW,
                            XmNleftAttachment,   XmATTACH_POSITION,
                            XmNleftOffset,       5,
                            XmNleftPosition,     35,
                            XmNrightAttachment,  XmATTACH_POSITION,
                            XmNrightOffset,      5,
                            XmNrightPosition,    65,
                            XmNbottomAttachment, XmATTACH_FORM,
                            XmNbottomOffset,     5,
                            NULL); 
   XtManageChild(scrTxtPrevious_PB);

   scrTxtNext_PB = XtVaCreateWidget("next_PB",
                            xmPushButtonWidgetClass,
                            scrTxt_MW,
                            XmNleftAttachment,   XmATTACH_POSITION,
                            XmNleftOffset,       5,
                            XmNleftPosition,     65,
                            XmNrightAttachment,  XmATTACH_POSITION,
                            XmNrightOffset,      5,
                            XmNrightPosition,    95,
                            XmNbottomAttachment, XmATTACH_FORM,
                            XmNbottomOffset,     5,
                            NULL); 
   XtManageChild(scrTxtNext_PB);


   separator = XtVaCreateWidget("separator",
                               xmSeparatorWidgetClass,
                               scrTxt_MW,
                               XmNbottomAttachment,  XmATTACH_WIDGET,
                               XmNbottomOffset,      5,
                               XmNbottomWidget,      scrTxtNext_PB,
                               XmNleftAttachment,    XmATTACH_FORM,
                               XmNleftOffset,        0,
                               XmNrightAttachment,   XmATTACH_FORM,
                               XmNrightOffset,       0,
                               NULL); 
   XtManageChild(separator);
   XtVaSetValues(XtParent(scrTxt_TF), 
                 XmNbottomAttachment,   XmATTACH_WIDGET,
                 XmNbottomOffset,       5,
                 XmNbottomWidget,       separator,
                 NULL);
   XtVaSetValues(head_RC, 
                 XmNbottomAttachment,   XmATTACH_WIDGET,
                 XmNbottomOffset,       5,
                 XmNbottomWidget,       separator,
                 NULL);
   XtUnmanageChild(head_RC);
   
   XtAddCallback(scrTxtNext_PB,     XmNactivateCallback, scrTxtNext_CB,     &loop);
   XtAddCallback(scrTxtPrevious_PB, XmNactivateCallback, scrTxtPrevious_CB, &loop);
   xmAddMouseEventHandler(scrTxt_TF);

   return scrTxt_MW;
}

/*******************************************************************/
/* Function scrTxtNext_CB()                                        */
/*                                                                 */
/* Cancel callback, set the loop flag                              */
/*                                                                 */  
/* Input: Widget w  not used                                       */
/*        XtPointer client_data the variable which is to be mod.   */
/*                                                                 */
/* return: -                                                       */
/*                                                                 */
/*******************************************************************/

void scrTxtNext_CB(Widget w, XtPointer clientData, XtPointer callData)
{
   *((int*)clientData) = 1;
}

/*******************************************************************/
/* Function scrTxtPrevious_CB()                                    */
/*                                                                 */
/* Cancel callback, set the loop flag                              */
/*                                                                 */  
/* Input: Widget w  not used                                       */
/*        XtPointer client_data the variable which is to be mod.   */
/*                                                                 */
/* return: -                                                       */
/*                                                                 */
/*******************************************************************/

void scrTxtPrevious_CB(Widget w, XtPointer clientData, XtPointer callData)
{
   *((int*)clientData) = -1;
}

/*******************************************************************/
/* Function popupScrolledTextWindow()                              */
/*                                                                 */
/* Popup the window                                                */
/*                                                                 */  
/* Input: Widget          old   the "parent" window                */
/*        wConfig_data_t *data                                     */
/*                                                                 */
/* return: -                                                       */
/*                                                                 */
/*******************************************************************/

void popupScrolledTextWindow(Widget old, wConfig_data_t* data)
{
   XmString xms;

   /* set buttons */
   if ( data->bt3 )
   {
      xms = XmStringCreateSimple(data->bt3);
      XtVaSetValues(scrTxtNext_PB, XmNlabelString, xms, NULL);
      XtSetSensitive(scrTxtNext_PB, True);
      XmStringFree(xms);
   }
   else
   {
      XtSetSensitive(scrTxtNext_PB, False);
   }

   if ( data->bt2)   
   {
      xms = XmStringCreateSimple(data->bt2);
      XtVaSetValues(scrTxtPrevious_PB, XmNlabelString, xms, NULL);
      XtManageChild(scrTxtPrevious_PB);
      XmStringFree(xms);
   }
   else
   {
      XtUnmanageChild(scrTxtPrevious_PB);
   }

   if ( data->message )
   {
      XtVaSetValues(scrTxt_TF, XmNvalue,data->message, NULL);
   }

   if ( data->colTb )
   {
      XtManageChild(head_RC);
      XtVaSetValues(XtParent(scrTxt_TF), 
                    XmNbottomAttachment,   XmATTACH_WIDGET,
                    XmNbottomOffset,       5,
                    XmNbottomWidget,       head_RC,
                    NULL);
   }
   else
   {
      XtUnmanageChild(head_RC);
      XtVaSetValues(XtParent(scrTxt_TF), 
                    XmNbottomAttachment,   XmATTACH_WIDGET,
                    XmNbottomOffset,       5,
                    XmNbottomWidget,       separator,
                    NULL);
   }
   if ( data->printerName && data->printerName[0] != '?' )
   {
      xms = XmStringCreateSimple(data->printerName);
      XtVaSetValues(title_LB, XmNlabelString, xms, NULL);
      XmStringFree(xms);
   }


   /* show the caller window */
   if ( old != scrTxt_MW )
   {
      XtUnmanageChild(old);
      XtManageChild(scrTxt_MW);
   }

   if ( data->wait )
   {
      *data->wait = 0;
      loop = 0;
      while(!loop)
         XtAppProcessEvent(theApp, XtIMAll);
      if ( data->colTb )
      {
         data->intVal = XmToggleButtonGetState(col_TB);
      }
      else
      {
         data->intVal = 0;
      }
   }
   if ( data->wait )
      *data->wait = loop;

   data->actWindow = scrTxt_MW;
}
