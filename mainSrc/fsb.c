/* file fsb.c
 *
 * File selection box
 *
 */
/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <sys/param.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <dirent.h>
#include <unistd.h>
#include <X11/Intrinsic.h>
#include <X11/StringDefs.h>
#include <X11/IntrinsicP.h>
#include <X11/Core.h>
#include <Xm/Xm.h>
#include <Xm/MwmUtil.h>
#include <Xm/FileSB.h>
#include <Xm/Form.h>
#include <Xm/Label.h>
#include <Xm/MessageB.h>
#include <Xm/TextF.h>
#include "wheelmouse.h"

static Widget targetTextField;
Widget fsb;

/*******************************************************************/ 
/*                                                                 */ 
/* NAME:      cancel_CB                                            */ 
/*                                                                 */ 
/* FUNCTION:  exit                                                 */
/*                                                                 */
/* INPUT:                                                          */
/*                                                                 */
/* OUTPUT:                                                         */ 
/*                                                                 */ 
/* RETURN:                                                         */ 
/*                                                                 */ 
/* REMARKS:                                                        */ 
/*                                                                 */ 
/*******************************************************************/ 

static void cancel_CB(Widget w, XtPointer a, XtPointer b)
{
   XtUnmanageChild(fsb);
}

/*******************************************************************/ 
/*                                                                 */ 
/* NAME:      ok_CB                                                */ 
/*                                                                 */ 
/* FUNCTION:  exit                                                 */
/*                                                                 */
/* INPUT:                                                          */
/*                                                                 */
/* OUTPUT:                                                         */ 
/*                                                                 */ 
/* RETURN:                                                         */ 
/*                                                                 */ 
/* REMARKS:                                                        */ 
/*                                                                 */ 
/*******************************************************************/ 

static void ok_CB(Widget w, XtPointer d, XtPointer c)
{
   XmFileSelectionBoxCallbackStruct *cbs =
                     (XmFileSelectionBoxCallbackStruct*)c;
   char *s = NULL; 
   char *t = NULL; 

   /* get value of widget */
   XmStringGetLtoR(cbs->value, XmSTRING_DEFAULT_CHARSET, &s);
   
   t = s;
   while ( *t )
   {
      if ( t[1] == '\0' && *t == '/' )
         break;
      t++;
   }

   if ( *t == '\0' )
   {
      XmTextFieldSetString(targetTextField,s);   
      XtUnmanageChild(fsb);
   }
   free(s);
}


/*******************************************************************/ 
/*                                                                 */ 
/* NAME:      createFileSelection                                  */ 
/*                                                                 */ 
/* FUNCTION:  create or popup the file selection box               */
/*                                                                 */
/* INPUT:                                                          */
/*                                                                 */
/* OUTPUT:                                                         */ 
/*                                                                 */ 
/* RETURN:                                                         */ 
/*                                                                 */ 
/* REMARKS:                                                        */ 
/*                                                                 */ 
/*******************************************************************/ 

Widget createFileSelection(Widget parent, Widget target)
{
    Arg           args[10];
    int           n;
    Widget wid;

    targetTextField = target;
    
    if ( fsb == NULL )
    {
       n = 0;
       XtSetArg(args[n], XmNdialogStyle,XmDIALOG_FULL_APPLICATION_MODAL ); n++;
       fsb = XmCreateFileSelectionDialog(parent, "fsb", args, n);
  
       /* not help button, ... here */
       wid = XmFileSelectionBoxGetChild(fsb, XmDIALOG_HELP_BUTTON);    
       if ( wid != NULL ) XtUnmanageChild(wid);
          XtManageChild(fsb);

       wid = XmFileSelectionBoxGetChild(fsb, XmDIALOG_FILTER_LABEL);    
       if ( wid != NULL ) XtUnmanageChild(wid);
          XtManageChild(fsb);
    
       wid = XmFileSelectionBoxGetChild(fsb, XmDIALOG_FILTER_TEXT);    
       if ( wid != NULL ) XtUnmanageChild(wid);
          XtManageChild(fsb);
    
       wid = XmFileSelectionBoxGetChild(fsb, XmDIALOG_SELECTION_LABEL);    
       if ( wid != NULL ) XtUnmanageChild(wid);
          XtManageChild(fsb);
    
       wid = XmFileSelectionBoxGetChild(fsb, XmDIALOG_TEXT);    
       if ( wid != NULL ) XtUnmanageChild(wid);
          XtManageChild(fsb);
    
       XtAddCallback(fsb,XmNcancelCallback, cancel_CB, NULL);
       XtAddCallback(fsb,XmNokCallback, ok_CB, NULL);
		 
		 /* whell mouse handler, we need the list for dir and file */
		 wid = XmFileSelectionBoxGetChild(fsb,XmDIALOG_DIR_LIST);
       if ( wid != NULL ) xmAddMouseEventHandler(wid);
		 wid = XmFileSelectionBoxGetChild(fsb,XmDIALOG_LIST);
       if ( wid != NULL ) xmAddMouseEventHandler(wid);
		 
    }
    else
    {
       XtManageChild(fsb);
    }
    
    return fsb;
}
