/* file cfg2.h
 *
 * Second configuration window, device
 */
extern void popupCfg2(Widget old, wConfig_data_t *data);

extern Widget createConfigureForm(Widget parent, int *next);
extern Widget autodetect_TB;
extern Widget tooltip_TB;

extern Widget cfg2Printer_PB;
extern Widget cfg2Device_PB;
