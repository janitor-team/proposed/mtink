/*
 * File mtink.h
 *
 * Copyright (C) 2001 Jean-Jacques Sarton jj.sarton@t-online.de
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef MTINK_H

#define MTINK_H

#if defined(__cplusplus) || defined(c_plusplus)
extern "C" {
#endif


#include "model.h"

#define ConfigFile ".mtinkrc"

#define QUERY_PRINTER 0
#define QUERY_DEVICE  1

#if WITH_X
typedef struct appResourceRec_s
{
   char *error;
   char *selfTest;
   char *busy;
   char *printing;
   char *ok;
   char *cleaning;
   char *unknown;
   char *addPrinterTxt;
   char *save;
   char *about;
   char *cancel;
   char *next;
   char *previous;
   char *ctTxt0;
   char *ctTxtC0;
   char *ctTxt1;
   char *ctTxt2;
   char *ctTxt3;
   char *ctTxt1_1;
   char *ctTxt1_2;
   char *ctTxt1_3;
   char *ctTxtP;
   char *ctTxtL;
   char *ctTxt4;
   char *ctTxt5;
   char *ctTxt6;
   char *ctTxt7;
   char *ctTxt8;
   char *ctTxt9;
   char *ctTxtC;
   char *exTxt0;
   char *exTxt00;
   char *exTxt1;
   char *exTxt2;
   char *exTxt3;
   
}appResourceRec_t;

extern appResourceRec_t appResourceRec;
#endif

extern configData_t actConfig;

#if WITH_X
#define CYCLE_TIME 1000

extern XtIntervalId tid;
extern void handleTi(XtPointer closure, XtIntervalId *tidp);

extern Widget topLevel;
extern Widget mainForm;
extern XtAppContext  theApp;
extern Display *display;

extern char *browser;
extern char *autodetect;
extern char *miniHelp;
extern int   configFileIdx;
extern int   doCyclicScan;
extern int   usePopup;
extern char *mainResource;
extern char  guiLanguage[];
extern Widget miconOK;

typedef struct wConfig_data_s
{
   char    *printerName;
   char    *dev;
   char    *bt1;
   char    *bt2;
   char    *bt3;
   char    *message;
   int     *wait;
   int      intVal;    /* return value */
   void    *ptrVal;    /* return value not to be freed, point to static data */
   int      colTb;
   int      wType;
   int      pass;
   int      choices;
   Widget   actWindow;
} wConfig_data_t;
#else
typedef struct wConfig_data_s
{
   char    *printerName;
   char    *dev;
   int      intVal;    /* return value */
   void    *ptrVal;    /* return value not to be freed, point to static data */
   int      colTb;
   int      wType;
   int      pass;
   int      choices;
} wConfig_data_t;

#endif

wConfig_data_t firstConfig;
wConfig_data_t newPrinter;

wConfig_data_t exchangeCartridge;
wConfig_data_t alignHead;

#if WITH_X
extern Widget createNoPrinterBox(char *);
extern int    decodeStatus(unsigned char *buf, int len);
extern void   SetSensitive(int flag);
extern void   refreshMainWindow(void);
#endif

#if defined(__cplusplus) || defined(c_plusplus)
}
#endif

#endif
