/****************************************************
 * File bldpcode.c
 *
 * write and read to / from printer
 *
 *
 * Copyright (C) 2001 Jean-Jacques Sarton jj.sarton@t-online.de
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 ****************************************************/

static unsigned char remote_hdr[] =
{
  033, '@', 033, '(', 'R', 010, 0, 0, 
  'R', 'E', 'M', 'O', 'T', 'E', '1'
};
static unsigned char new_remote_hdr[] =
{
  033, '(', 'R', 010, 0, 0, 
  'R', 'E', 'M', 'O', 'T', 'E', '1'
};

static unsigned char remote_trailer[] = {
   033, 0, 0, 0
};

/******************************************************
 * Function: add_newlines()
 *
 * add count line feeds 
 *
 ******************************************************/

static int add_newlines(int count, int bufpos, unsigned char *printer_cmd)
{
  int i;
  for (i = 0; i < count; i++)
    {
      printer_cmd[bufpos++] = '\r';
      printer_cmd[bufpos++] = '\n';
    }
    return bufpos;
}

/******************************************************
 * Function: add_formfeed()
 *
 * add code for line feed to the printer command buffer
 *
 ******************************************************/

static int add_formfeed(int bufpos, unsigned char *printer_cmd)
{
   printer_cmd[bufpos++] =  '\f';
   return bufpos;
}



/******************************************************
 * Function: do_remote_cmd()
 *
 * build the command for the printer according to the
 * given parameters
 *
 ******************************************************/

static int do_remote_cmd_new(int bufpos,  unsigned char *printer_cmd,
                         const char *cmd, int nargs, ...)
{
   int i;
   va_list args;
   va_start(args, nargs);

   memcpy(printer_cmd + bufpos, new_remote_hdr, sizeof(new_remote_hdr));
   bufpos += sizeof(new_remote_hdr);

   memcpy(printer_cmd + bufpos, cmd, 2);
   bufpos += 2;

   printer_cmd[bufpos] = nargs % 256;
   printer_cmd[bufpos + 1] = (nargs >> 8) % 256;
   if (nargs > 0)
      for (i = 0; i < nargs; i++)
         printer_cmd[bufpos + 2 + i] = va_arg(args, int);
   bufpos += 2 + nargs;

   memcpy(printer_cmd + bufpos, remote_trailer, sizeof(remote_trailer));
   bufpos += sizeof(remote_trailer);
   return bufpos;
}

static int do_remote_cmd(int bufpos,  unsigned char *printer_cmd,
                         const char *cmd, int nargs, ...)
{
   int i;
   va_list args;
   va_start(args, nargs);

   memcpy(printer_cmd + bufpos, remote_hdr, sizeof(remote_hdr));
   bufpos += sizeof(remote_hdr);

   memcpy(printer_cmd + bufpos, cmd, 2);
   bufpos += 2;

   printer_cmd[bufpos] = nargs % 256;
   printer_cmd[bufpos + 1] = (nargs >> 8) % 256;
   if (nargs > 0)
      for (i = 0; i < nargs; i++)
         printer_cmd[bufpos + 2 + i] = va_arg(args, int);
   bufpos += 2 + nargs;

   memcpy(printer_cmd + bufpos, remote_trailer, sizeof(remote_trailer));
   bufpos += sizeof(remote_trailer);
   return bufpos;
}

/******************************************************
 * Function: do_remote_cmd()
 *
 * build the command for the printer according to the
 * given parameters
 *
 ******************************************************/

static int do_remote_cmd_first(int bufpos,  unsigned char *printer_cmd,
                         const char *cmd, int nargs, ...)
{
   int i;
   va_list args;
   va_start(args, nargs);

   memcpy(printer_cmd + bufpos, new_remote_hdr, sizeof(new_remote_hdr));
   bufpos += sizeof(new_remote_hdr);

   memcpy(printer_cmd + bufpos, cmd, 2);
   bufpos += 2;

   printer_cmd[bufpos] = nargs % 256;
   printer_cmd[bufpos + 1] = (nargs >> 8) % 256;
   if (nargs > 0)
      for (i = 0; i < nargs; i++)
         printer_cmd[bufpos + 2 + i] = va_arg(args, int);
   bufpos += 2 + nargs;

   return bufpos;
}

static int do_remote_cmd_add(int bufpos,  unsigned char *printer_cmd,
                         const char *cmd, int nargs, ...)
{
   int i;
   va_list args;
   va_start(args, nargs);

   memcpy(printer_cmd + bufpos, cmd, 2);
   bufpos += 2;

   printer_cmd[bufpos] = nargs % 256;
   printer_cmd[bufpos + 1] = (nargs >> 8) % 256;
   if (nargs > 0)
      for (i = 0; i < nargs; i++)
         printer_cmd[bufpos + 2 + i] = va_arg(args, int);
   bufpos += 2 + nargs;

   return bufpos;
}

static int do_remote_cmd_last(int bufpos,  unsigned char *printer_cmd,
                         const char *cmd, int nargs, ...)
{
   int i;
   va_list args;
   va_start(args, nargs);

   memcpy(printer_cmd + bufpos, cmd, 2);
   bufpos += 2;

   printer_cmd[bufpos] = nargs % 256;
   printer_cmd[bufpos + 1] = (nargs >> 8) % 256;
   if (nargs > 0)
      for (i = 0; i < nargs; i++)
         printer_cmd[bufpos + 2 + i] = va_arg(args, int);
   bufpos += 2 + nargs;

   memcpy(printer_cmd + bufpos, remote_trailer, sizeof(remote_trailer));
   bufpos += sizeof(remote_trailer);

   return bufpos;
}

